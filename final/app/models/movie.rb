class Movie < ActiveRecord::Base
has_many :ratings
validates :title, presence: true, Length: { minimum: 4 }
end
