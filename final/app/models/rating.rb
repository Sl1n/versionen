class Rating < ActiveRecord::Base
  belongs_to :movie
  validates :stars, presence: true, numericality: { only_integer: true,  greater_than: 0, less_than: 6 }
end
