class RatingsController < ApplicationController
  def show
    @rating = Rating.find(params[:id])
  end

  def create
    @movie = Movie.find(params[:movie_id])
    @rating = @movie.ratings.create(rating_params)
    redirect_to movie_rating_path(@movie, @rating)
  end

  private
    def rating_params
      params.require(:rating).permit(:stars, :message)
    end
end
