class ConnectMoviesRatings < ActiveRecord::Migration
  def change
    change_table :ratings do |t|
        t.references :movie, index: true, foreign_key: true
    end
  end
end
