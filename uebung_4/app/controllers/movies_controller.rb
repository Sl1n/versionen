class MoviesController < ApplicationController
  def new
  end

  def create
    @movie = Movie.new(movie_params)

    @movie.save
    redirect_to @movie
  end

private
      def movie_params
        params.require(:movie).permit(:title, :director, :year)
      end
end
