class Rating < ActiveRecord::Base
  validates :stars, presence: true, numericality: { only_integer: true,  greater_than: 0, less_than: 6 }
end
